<?php
/**
 * No Framework, CORE PHP ONLY.
 * Author: Raymond S. Usbal <raymond@philippinedev.com>
 * Date: 4 September 2014
 */

/**
 * Show errors for debugging purposes.  Comment out for production.
 */
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', '1'); 

/**
 * Session is used for login and messages.
 */
session_start();
header("Content-Type: text/html");

/**
 * Settings for this application.
 */
require 'system/settings.php';

/**
 * Notification, error messages stored on session thru $message variable.
 */
require 'system/messages.php'; 

/**
 * Simple router solution.
 */
require 'system/application/init/router.php';

if (isset($view['template'])) {
    /**
     * Renders HTML template using view data on $view.
     */
    require 'system/application/init/template.php';
} else {
    /**
     * Ajax results are simply echoed.
     */
    echo $view;
}

