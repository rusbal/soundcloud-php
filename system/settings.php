<?php 
/**
 * No Framework, CORE PHP ONLY.
 * Author: Raymond S. Usbal <raymond@philippinedev.com>
 * Date: 4 September 2014
 */

/**
 * Secrets
 */
require 'system/application/secrets/confidential.php';

/**
 * This file is empty unless the user changed his password.
 */
if (file_exists("system/application/data/password")) {
    $USER_PASSWORD_HASH   = file_get_contents("system/application/data/password");
}

/**
 * Settings
 */
$APP_HOSTNAME             = 'http://mvc.loc';
$APP_HOSTNAME             = isset($APP_HOSTNAME) ? $APP_HOSTNAME : 'http://ancient-meadow-4277.herokuapp.com';

$user_email               = $secret_user_email ? $secret_user_email : 'invalid_email';
$user_password_hash       = isset($USER_PASSWORD_HASH) ? $USER_PASSWORD_HASH : $secret_user_password_hash_hard;
$user_nickname            = isset($_SESSION['soundcloud_token']) ? $_SESSION['user']['nickname'] : 'Disconnected User';

$_settings = array( 
    'protocol'                 => $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://",
    'domain'                   => $_SERVER['HTTP_HOST'],

    'SECRET_KEY'               => $SECRET_KEY,
    'APP_HOSTNAME'             => $APP_HOSTNAME,
    'SOUND_CLOUD_ACCESS_TOKEN' => $SOUND_CLOUD_ACCESS_TOKEN,

    'user_email'               => $user_email,
    'user_password_hash'       => $user_password_hash,
    'user_nickname'            => $user_nickname,

    'allowed_emails'           => array($user_email),

    'user_information'         => array(
        'user_email' => array(
            'password_hash'    => $user_password_hash,
            'nickname'         => $user_nickname
        )
    ),

    'client_id'                => $secret_client_id,
    'client_secret'            => $secret_client_secret,

    'logged_nickname'          => $secret_nickname,
);

