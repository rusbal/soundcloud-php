<?php 
/**
 * No Framework, CORE PHP ONLY.
 * Author: Raymond S. Usbal <raymond@philippinedev.com>
 * Date: 4 September 2014
 */

/**
 * Define route paths here
 */

$routes = array(

    # ----------------------   -----------------   ---------------------   ---------
    # URL                      # CONTROLLER FILE   # METHOD                # REQUEST
    # ----------------------   -----------------   ---------------------   ---------
    
    '/login'                => array('auth',       'login',                'GET'),
    '/login_user_pass'      => array('auth',       'login_user_pass',      'POST'),
    '/logout'               => array('auth',       'logout',               'GET'),
    '/reset_password'       => array('auth',       'reset_password',       'GET'),
    '/reset_password_post'  => array('auth',       'reset_password_post',  'POST'),
    '/sound_cloud_redirect' => array('soundcloud', 'sound_cloud_redirect', 'GET'),
    '/sc_authorize'         => array('soundcloud', 'sc_authorize',         'GET'),
    '/'                     => array('soundcloud', 'home',                 'GET'),
    '/spreadsheet'          => array('soundcloud', 'spreadsheet',          'GET'),
    '/spreadsheet_load'     => array('soundcloud', 'spreadsheet_load',     'GET'),
    '/spreadsheet_save'     => array('soundcloud', 'spreadsheet_save',     'POST'),
    '/sc_disconnect'        => array('soundcloud', 'sc_disconnect',        'GET'),
    '/playlists'            => array('soundcloud', 'playlists',            'GET'),
    '/sc_play'              => array('soundcloud', 'sc_play',              'GET'),
    '/col_size_save'        => array('soundcloud', 'col_size_save',        'POST'),
);

