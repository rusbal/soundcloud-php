<?php
/**
 * No Framework, CORE PHP ONLY.
 * Author: Raymond S. Usbal <raymond@philippinedev.com>
 * Date: 4 September 2014
 */

/**
 * For storing messages.
 */

class Message {
    private $_info;
    private $_warning;
    private $_error;
    private $_success;

    public function stash_messages($restore=false) {
        if ($restore) {
            $_SESSION['messages']            = array();
            $_SESSION['messages']['info']    = $this->_info;
            $_SESSION['messages']['warning'] = $this->_warning;
            $_SESSION['messages']['error']   = $this->_error;
            $_SESSION['messages']['success'] = $this->_success;
        } else {
            $this->_info    = $_SESSION['messages']['info'];
            $this->_warning = $_SESSION['messages']['warning'];
            $this->_error   = $_SESSION['messages']['error'];
            $this->_success = $_SESSION['messages']['success'];
        }
    }

    public function clear_all() {
        $_SESSION['messages']            = array();
        $_SESSION['messages']['info']    = array();
        $_SESSION['messages']['warning'] = array();
        $_SESSION['messages']['error']   = array();
        $_SESSION['messages']['success'] = array();
        $_SESSION['messages']['data']    = array();
    }

    public function push($type, $msg) {
        $_SESSION['messages'][$type][] = $msg;
    }

    public function push_data($type, $msg) {
        $_SESSION['messages'][$type][] = $msg;
    }

    private function _pop_info() {
        $msgs = $_SESSION['messages']['info'];
        $_SESSION['messages']['info'] = array();
        return $msgs;
    }

    private function _pop_warning() {
        $msgs = $_SESSION['messages']['warning'];
        $_SESSION['messages']['warning'] = array();
        return $msgs;
    }

    private function _pop_error() {
        $msgs = $_SESSION['messages']['error'];
        $_SESSION['messages']['error'] = array();
        return $msgs;
    }

    private function _pop_success() {
        $msgs = $_SESSION['messages']['success'];
        $_SESSION['messages']['success'] = array();
        return $msgs;
    }

    public function pop_all() {
        return array(
            'info'    => $this->_pop_info(),
            'warning' => $this->_pop_warning(),
            'danger'  => $this->_pop_error(),
            'success' => $this->_pop_success(),
        );
    }

    public function pop_data() {
        $msgs = $_SESSION['messages']['data'];
        $_SESSION['messages']['data'] = array();
        return $msgs;
    }
} 

$message = new Message();

if (!isset($_SESSION['messages'])) {
    $message->clear_all();
}

