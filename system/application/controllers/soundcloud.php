<?php
/**
 * No Framework, CORE PHP ONLY.
 * Author: Raymond S. Usbal <raymond@philippinedev.com>
 * Date: 4 September 2014
 */

require 'system/lib/Soundcloud/Service.php';
require 'system/lib/Soundcloud/Version.php';

class Soundcloud {

    private $client;
    private $default_col_sizes = array(50, 250, 400, 150, 150, 150, 150, 600, 150, 150, 150, 150, 150, 150, 150); 
    private $reset_client      = false;

    function __construct() {
        global $_settings;

        if (isset($_SESSION['soundcloud_token'])) {
            $this->client = new Soundcloud\Service($_settings['client_id'], $_settings['client_secret']);

            if ($_SESSION['soundcloud_token']['expires_on'] <= time()) {
                $_SESSION['soundcloud_token'] = $this->client->accessTokenRefresh($_SESSION['soundcloud_token']['access_token']);
            } else {
                $this->client->setAccessToken($_SESSION['soundcloud_token']['access_token']);
            }

        } else {
            $sc_redirect_client = $_settings['protocol'] . $_settings['domain'] . '/sound_cloud_redirect';
            $this->client = new Soundcloud\Service($_settings['client_id'], $_settings['client_secret'], $sc_redirect_client);
        }
    }

    public function home() { 
        $this->_check_auth_redirect();

        $me = json_decode($this->client->get('me'));
        $_SESSION['soundcloud_username'] = $me->username; 

        header('Location: ' . $_settings['protocol'] . $_settings['domain'] . 'spreadsheet');
    }

    public function sc_authorize() {
        global $_settings;

        return array(
            'parent_template' => 'layout.php',
            'template' => 'authorize.php',
            'data'     => array(
                'authorize_url'  => $this->client->getAuthorizeUrl(),
            ),
        );
    }

    public function sound_cloud_redirect() {
        global $_settings;

        $response                                     = $this->client->accessToken($_GET['code']);
        $_SESSION['soundcloud_token']               = $response;
        $_SESSION['soundcloud_token']['expires_on'] = time() + $response['expires_in'];

        $this->reset_client = true;

        header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/');
    }

    public function spreadsheet() {
        $this->_check_auth_redirect();

        return array(
            'parent_template' => 'layout.php',
            'template' => 'spreadsheet.php',
            'extra_header_template' => true,
            'finishing_block_template' => true,
            'data'     => array(
                'playlist'  => (isset($_GET['playlist']) ? $_GET['playlist'] : ''),
                'p'         => null,
                'col_sizes' => json_encode($this->default_col_sizes),
            ),
        );
    }

    public function spreadsheet_load() {
        $this->_check_auth_redirect();

        $playlist = isset($_GET['playlist']) ? $_GET['playlist'] : '';
        if ($playlist != '') {
            try {
                $tracks = $this->_get('playlists/'.$playlist.'/tracks', array(), array(), false);
            } catch (Services_Soundcloud_Invalid_Http_Response_Code_Exception $e) { 
                $tracks = array();
            } 
        } else {
            $tracks = array();
        }

        if (!$tracks) {
            $tracks = $this->_load_all_tracks();
        }

        $tracks_data = $this->_format_track($tracks);

        return json_encode(array(
            'data' => $tracks_data,
        ));
    }

    public function spreadsheet_save() { 
        $this->_check_auth_redirect();

        $data = json_decode($_POST['data']);
        if (!$data) {
            return json_encode(array(
                'status'  => 'error',
                'message' => 'Data not found',
            ));
        }

        $error_cells   = array();
        $success_cells = array();

        try {
            foreach ($data as $datum) {
                $track_id      = $datum->track_id;
                $row           = $datum->change_data[0];
                $attr          = $datum->change_data[1];
                $prev_value    = $datum->change_data[2];
                $current_value = $datum->change_data[3];

                if ($prev_value == $current_value) {
                    continue;
                }

                // fetch a track by it's ID
                $track = json_decode($this->client->get('tracks/'.$track_id));

                if ($attr == 'release_date') {
                    try {
                        $d = strtotime($current_value);
                        $this->client->put('tracks/' . $track->id, array(
                            'track[release_day]'   => date('j', $d),
                            'track[release_month]' => date('n', $d),
                            'track[release_year]'  => date('Y', $d)
                        ));

                        $success_cells[] = array('row' => $row, 'attribute' => $attr);
                    } catch(Exception $e) {
                        $error_cells[]   = array('row' => $row, 'attribute' => $attr);
                    } 
                    continue;
                }

                if ($this->_is_valid($attr, $current_value)) { 

                    // update the track's metadata
                    $this->client->put('tracks/' . $track->id, array(
                        'track['.$attr.']' => $current_value,
                    ));

                    $success_cells[] = array('row' => $row, 'attribute' => $attr);
                } else {
                    $error_cells[]   = array('row' => $row, 'attribute' => $attr);
                } 
            }

            $resp = array('status' => 'success', 'message' => 'Successfully saved.');
            if ($success_cells) {
                $resp['success_cells'] = $success_cells;
            }

            if ($error_cells) {
                $resp['error_cells'] = $error_cells;
            }

            return json_encode($resp);

        } catch(Exception $e) {
            return json_encode(array(
                'status'  => 'error',
                'message' => 'Value error while parsing json data',
            ));
        }
    }

    public function sc_disconnect() {
        global $message;

        $this->_check_auth_redirect();

        if ($_GET['confirm'] == '1') {
            unset($_SESSION['soundcloud_token']);
            unset($_SESSION['soundcloud_username']);

            $message->push('info', 'SoundCloud account disconnected');
            header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/');
        }

        return array(
            'parent_template' => 'layout.php',
            'template' => 'disconnect.php',
        );
    }

    public function playlists() {
        $this->_check_auth_redirect();

        if (!isset($_SESSION['soundcloud_token'])) {
            header('Location: ' . $_settings['protocol'] . $_settings['domain'] . 'sc_authorize');
            exit;
        }

        return array(
            'parent_template' => 'layout.php',
            'template' => 'playlists.php',
            'data'     => array(
                'playlists' => $this->_get('me/playlists/', array(), array(), false),
            ),
        );
    }

    public function sc_play() {
        $this->_check_auth_redirect();

        $track_id = isset($_GET['track_id']) ? $_GET['track_id'] : '';

        $track = $this->_get('me/tracks/'.$track_id, array(), array(), false);
        $param = array(
            'url'       => $track['uri'],
            'maxheight' => 400,
        );
        $embed_info = $this->_get('oembed', $param, array(), false);

        return $embed_info['html'];
    }

    public function col_size_save() {
        $this->_check_auth_redirect();

        $col = $_POST['col'];
        if ($col == '') {
            return json_encode(array(
                'status'  => 'error',
                'message' => 'Invalid column',
            ));
        }

        try {
            $col = (int) $col;
        } catch(Exception $e) {
            return json_encode(array(
                'status'  => 'error',
                'message' => 'Invalid column',
            ));
        }

        $size = $_POST['size'];
        if ($size == '') {
            return json_encode(array(
                'status'  => 'error',
                'message' => 'Invalid size',
            ));
        }

        try {
            $size = (int) $size;
        } catch(Exception $e) {
            return json_encode(array(
                'status'  => 'error',
                'message' => 'Invalid size',
            ));
        }

        $this->_save_col_size($col, $size);

        return json_encode(array(
            'status' => 'error',
            'col'    => $col,
            'size'   => $size,
        ));
    }

    private function _check_auth_redirect() {
        global $message;

        if (!isset($_SESSION['user'])) {
            $message->push('error', "Please login");
            header('Location: ' . $_settings['protocol'] . $_settings['domain'] . 'login');
            exit;
        }

        if (!isset($_SESSION['soundcloud_token'])) {
            header('Location: ' . $_settings['protocol'] . $_settings['domain'] . 'sc_authorize');
            exit;
        }
    }

    private function _is_valid($attr, $value) {
        if ($attr == 'track_type') {
            $valid_track_types = array(
                'original',
                'remix',
                'live',
                'recording',
                'spoken',
                'podcast',
                'demo',
                'in progress',
                'stem',
                'loop',
                'sound effect',
                'sample',
                'other',
                ''
            );
            return in_array($value, $valid_track_types);
        } 
        if ($attr == 'purchase_url' || $attr == 'video_url') {
            return (filter_var($value, FILTER_VALIDATE_URL));
        } 
        return true;
    }

    private function _save_col_size($col, $size) {
        $col_sizes_str = $_POST['COLUMNS_SIZE'];
        if ($col_sizes_str == '') {
            $col_size = $this->default_col_sizes;
        }

        $col_size = $col_sizes_str.split(',');
        if (count($col_size) != 15) {
            $col_size = $this->default_col_sizes;
            return json_encode(array(
                'status'  => 'error',
                'message' => 'Length size does not match',
            ));
        }

        try {
            $in_col_size = $col_size;
            foreach ($in_col_size as $key => $value) {
                $col_size[$key] = (int) $value;
            }
        } catch(Exception $e) {
            $col_size = $this->default_col_sizes;
        }

        $col_size[$col] = $size;

        $col_size_string = '';
        foreach ($col_size as $cs) {
            $col_size_string .= ($col_size_string ? ',' : '') . $cs;
        }
        putenv("COLUMNS_SIZE=$col_size_string");
    } 

    private function _get($path, $params=array(), $curlOptions=array(), $decode=true) {
        try {
            $result = json_decode($this->client->get($path, $params, $curlOptions), true);
        } catch (Services_Soundcloud_Invalid_Http_Response_Code_Exception $e) {
            exit($e->getMessage());
        }

        if ($decode) {
            return json_decode($result, true);
        } else {
            return $result;
        }
    }

    private function _load_all_tracks() {
        $result = $this->_get('me');
        $track_count = $result["track_count"];
        $page_size = 50;

        $pages = (int) ceil((float) $track_count / (float) $page_size);
        $all_tracks = array();

        foreach (range(1, $pages + 1) as $page) {
            $params = array(
                'limit'  => $page_size,
                'offset' => (($page - 1) * $page_size),
            );
            $tracks = $this->_get('me/tracks', $params, array(), false);
            $all_tracks += $tracks;
        }

        return $all_tracks;
    }


    private function _format_track($tracks) {
        $tracks_data = array();

        foreach ($tracks as $t) {
            if (!$t['release_month'] && !$t['release_day'] && !$t['release_year']) {
                $release_date = '';
            } else {
                $month = $t['release_month'] ? $t['release_month'] : '0';
                $day = $t['release_day'] ? $t['release_day'] : '0';
                $year = $t['release_year'] ? $t['release_year'] : '0000';
                $release_date = "$month/$day/$year";
            }

            $tracks_data[] = array(
                'id'            => $t['id'],
                'title'         => $t['title'],
                'description'   => $t['description'],
                'track_type'    => $t['track_type'],
                'genre'         => $t['genre'],
                'tag_list'      => $t['tag_list'],
                'label_name'    => $t['label_name'],
                'release_date'  => $release_date,
                'release'       => $t['release'],
                'isrc'          => $t['isrc'],
                'bpm'           => $t['bpm'],
                'key_signature' => $t['key_signature'],
                'purchase_url'  => $t['purchase_url'],
                'video_url'     => $t['video_url'],
            ); 
        }

        return $tracks_data;
    }
}

