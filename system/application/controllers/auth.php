<?php
/**
 * No Framework, CORE PHP ONLY.
 * Author: Raymond S. Usbal <raymond@philippinedev.com>
 * Date: 4 September 2014
 */

class Auth {

    public function login() {
        return array(
            'template' => 'login.php',
        );
    }

    public function login_user_pass() {
        global $_settings, $message;

        if (isset($_SESSION['user'])) {
            header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/');
            exit;
        }

        $email = $_POST['email'];
        $password = $_POST['password'];

        if (!in_array($email, $_settings['allowed_emails'])) {
            $message->push('error', "$email not in allowed emails");
            header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/login');
            exit;
        }
        
        if ($this->_verify_access($password, $email)) {
            $_SESSION['user'] = array(
                'id'       => $email,
                'email'    => $email,
                'nickname' => $_settings['logged_nickname'],
            );
            header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/');
            exit;
        }

        $message->push('data', array('error_code', 'invalid_login'));
        header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/login');
    }

    public function logout() {
        global $_settings, $message; 
        $message->stash_messages(); 
        $_SESSION = array();
        $message->stash_messages(true); 
        header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/login');
    }

    public function reset_password() {
        return array(
            'parent_template' => 'layout.php',
            'template' => 'reset_password.php',
        );
    }

    public function reset_password_post() {
        global $message, $_settings;

        $current_password    = $_POST['current_password'];
        $new_password        = $_POST['new_password'];
        $new_password_retype = $_POST['new_password_retype'];

        if (!$this->_verify_access($current_password, $_SESSION['user']['email'])) {
            $message->push('error', "Current password doesn't match. Please try again");
            header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/reset_password');
            exit;
        }

        if ($new_password != $new_password_retype) {
            $message->push('error', "Please retype password correctly");
            header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/reset_password');
            exit;
        }

        if (strlen($new_password) < 6) {
            $message->push('error', "New password is too short. Needs to be at least 6 char long");
            header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/reset_password');
            exit;
        }

        $password_hash = $this->_password_hash($new_password, $_SESSION['user']['email']);

        $myfile = fopen("system/application/data/password", "w") or die("Unable to open password file! Solution: $ chmod 0777 system/application/data/");
        fwrite($myfile, $password_hash);
        fclose($myfile);

        $_settings['user_information']['user_email']['password_hash'] = $password_hash;

        $message->push('success', "Password changed. Please login with new password");
        header('Location: ' . $_settings['protocol'] . $_settings['domain'] . '/logout');
    }

    private function _verify_access($password, $email) {
        global $_settings;
        return $this->_password_hash($password, $email) == $_settings['user_information']['user_email']['password_hash'];
    }

    private function _password_hash($password, $email) {
        return sha1(crypt($password, $email));
    }
}
