<?php 
/**
 * No Framework, CORE PHP ONLY.
 * Author: Raymond S. Usbal <raymond@philippinedev.com>
 * Date: 4 September 2014
 */

function res ($path) {
    return "/static/$path";
}

function url_for($path) {
    if ($path == 'home') {
        $path = '';
    }
    return "/$path";
}

function sc_username() {
    global $_settings;
    return isset($_SESSION['soundcloud_username']) ? $_SESSION['soundcloud_username'] : $_settings['user_nickname'];
}

function sc_is_connected() {
    return isset($_SESSION['soundcloud_username']);
}
