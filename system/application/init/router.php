<?php
/**
 * No Framework, CORE PHP ONLY.
 * Author: Raymond S. Usbal <raymond@philippinedev.com>
 * Date: 4 September 2014
 */

require 'system/router.php';

$url_no_query_string = strtok($_SERVER["REQUEST_URI"], '?');

$router_def_match = false;

if (array_key_exists($url_no_query_string, $routes)) {

    $requested_route = $routes[$url_no_query_string];
    list($controller, $method, $request_type) = $requested_route;

    if ($request_type == $_SERVER['REQUEST_METHOD']) {
        $router_def_match = true;
    }
}

if (!$router_def_match) { 
    echo '<p>Page <strong>' . $url_no_query_string . '</strong> not found.</p>';
    exit;
}

require "system/application/controllers/$controller.php";
$controller = ucfirst($controller);
$cont = new $controller();
$view = $cont->$method();

if (!$view) {
    echo '<p>No view for <strong>' . $url_no_query_string . '</strong> was set.</p>';
    exit;
} 

