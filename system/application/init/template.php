<?php
/**
 * No Framework, CORE PHP ONLY.
 * Author: Raymond S. Usbal <raymond@philippinedev.com>
 * Date: 4 September 2014
 */

require "system/application/tpl_functions.php";

$context = array();

$data = $message->pop_data();
foreach ($data as $datum) {
    $context[$datum[0]] = $datum[1];
}

$context['messages'] = $message->pop_all();
$context['data']     = isset($view['data']) ? $view['data'] : null;

if (isset($_SESSION['user'])) {
    $context['data']['user'] = array(
        'is_authenticated' => true,
        'id'               => $_SESSION['user']['id'],
        'email'            => $_SESSION['user']['email'],
        'nickname'         => $_SESSION['user']['nickname'],
    );
}

extract($context);

if (isset($view['parent_template'])) {
    require 'system/application/templates/'.$view['parent_template'];
} else {
    require 'system/application/templates/'.$view['template'];
}

