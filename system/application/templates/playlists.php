    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="page-header">
                <h2 class="text-center">Playlists</h2>
            </div>
<?php if ($data['playlists']): ?>
                <table class="table table-bordered">
                    <tr>
                        <th>Title</th>
                        <th>&nbsp;</th>
                    </tr>
    <?php foreach ($data['playlists'] as $p): ?>
        <?php if ($p['title']): ?>
                        <tr>
                        <td><?php echo $p['title']; ?></td>
                            <td>
                            <a class="btn btn-sm btn-primary" href="<?php echo url_for('spreadsheet'); ?>?playlist=<?php echo $p['id']; ?>">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                            </td>
                        </tr>
        <?php else: ?>
                        <tr>
                            <td colspan="2">Not one playlist found</td>
                        </tr>
        <?php endif; ?>
    <?php endforeach; ?>
                </table>
<?php else: ?>
                <div class="">No playlist found</div>
<?php endif; ?>

        </div>
    </div>
