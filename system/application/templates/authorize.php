    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            No soundcloud account is connected.
            <a href="<?php echo $data['authorize_url']; ?>">Authorize a soundcloud account</a>
        </div>
    </div>
