    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="confirm-message">
                <p>Are you sure you want to disconnect current account?</p>

                <div class="pull-left"><a href="<?php echo url_for('home'); ?>" class="btn btn-default">Cancel</a></div>
                <form class="form pull-right" action="<?php echo url_for('sc_disconnect'); ?>" method="get">
                    <input type="hidden" name="confirm" value="1">
                    <input class="btn btn-danger" type="submit" value="Disconnect">
                </form>
            </div>
        </div>
    </div>
