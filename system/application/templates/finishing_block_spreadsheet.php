    <script src="<?php echo res('handsontable-0.10.5/jquery.handsontable.full.js'); ?>"></script>
    <script src="<?php echo res('jquery-ui-1.10.3/js/jquery-ui.custom.min.js'); ?>"></script>
    <script src="<?php echo res('json2.js'); ?>"></script>
    <script type="text/javascript">

    var error_background = "#ffb3aa";
    var success_background = "#fff";
    var handsontable;

    var MESSAGE_SUCCESS = 1;
    var MESSAGE_ERROR = 2;

    function loadData() {
        $('#container').after('<div class="alert alert-info" style="position: absolute; z-index: 100; top: 50%; left: 45%" id="loading">Loading...</div>');
        $.ajax({
            url: "<?php echo url_for('spreadsheet_load'); ?>",
            dataType: 'json',
            data: {playlist: '<?php echo $playlist; ?>' },
            type: 'GET',
            success: function (res) {
                $('#loading').remove();
                handsontable.loadData(res.data);

            }
        });
    }

    var newwindow;
    function createPop(url, name) {
        newwindow = window.open(url, name, 'width=600,height=420,toolbar=0,menubar=0,location=0');
        if (window.focus) {
            newwindow.focus();
        }
    }

    var descriptionRenderer = function (instance, td, row, col, prop, value, cellProperties) {
        var track_id = Handsontable.helper.stringify(value);
        var url = '<?php echo url_for('sc_play'); ?>' + '?track_id=' + track_id;
        td.innerHTML = '<div class="text-center"><a class="play-btn btn btn-xs btn-default" href="' + url
                + '"><span class="glyphicon glyphicon-play"></span> </a></div>';

        return td;
    };

    $(document).ready(function () {
        var $container = $("#container");
        var columns = [
            {data: 'id', readOnly: true, renderer: descriptionRenderer},
            {data: 'title'},
            {data: 'description'},
            {
                data: 'track_type',
                type: 'dropdown',
                source: ['original', 'remix', 'live', 'recording', 'spoken', 'podcast', 'demo', 'in progress', 'stem',
                    'loop', 'sound effect', 'sample', 'other']
            },
            {data: 'genre'},
            {data: 'bpm'},
            {data: 'key_signature'},
            {data: 'tag_list'},
            {data: 'label_name'},
            {data: 'release_date', type: 'date', dateFormat: 'mm/dd/yy' },
            {data: 'release'},
            {data: 'isrc'},
            {data: 'purchase_url'},
            {data: 'video_url'},
            {data: 'id', readOnly: true}
        ];

        var column_reverse = {};
        $.each(columns, function (index, element) {
            column_reverse[element['data']] = index;
        });

        var status = {
            error_cells: {},
            success_cells: {},
            add_error: function (row, col) {
                var key = (row + '-' + col);
                if (this.success_cells[key]) {
                    delete  this.success_cells[key];
                }

                this.error_cells[key] = {row: row, col: col}
            },
            add_success: function (row, col) {
                var key = (row + '-' + col);
                if (this.error_cells[key]) {
                    delete  this.error_cells[key];
                }

                this.success_cells[key] = {row: row, col: col}
            }
        };

        var x = $container.handsontable({
            minSpareRows: 0,
            colHeaders: ['Play', 'Title', 'Description', 'Type', 'Genre', 'BPM', 'Key', 'Tag List', 'Record Label',
                'Release Date', 'Release Number', 'ISRC', 'Buy Link', 'Video Link', 'SoundCloud ID'],
            fixedColumnsLeft: 2,
            fixedColumnsTop: 0,
            columnSorting: true,
            height: calculateHeight(),
            columns: columns,
            stretchH: 'all',
            contextMenu: false,
            manualColumnResize: true,
            afterColumnResize: function (col, size) {
                $.ajax({
                    url: '<?php echo url_for('col_size_save'); ?>',
                    dataType: 'json',
                    type: 'POST',
                    data: {col: col, size: size},
                    success: function (res) {
                        if (res['status'] == 'success') {
                            notify_user(MESSAGE_SUCCESS, 'Column size saved');
                        }
                        if (res['status'] == 'error') {
                            notify_user(MESSAGE_ERROR, 'Column size was not saved');
                        }
                    }
                });

            },
                colWidths: <?php echo $data['col_sizes']; ?>,
            startRows: 1,
            afterChange: function (change, source) {
                if (source === 'loadData') {
                    return;
                }
                // {# Change - autofill, undo, paste, edit #}

                var data = handsontable.getData();
                var send_data = [];

                for (var c in change) {
                    var ch = change[c];
                    var id = data[ch[0]]['id'];
                    if (ch[2] == ch[3]) {
                        continue;
                    }
                    console.log(ch);
                    send_data.push({
                        'track_id': data[ch[0]]['id'],
                        'change_data': ch
                    });
                }

                if (send_data.length == 0) {
                    return;
                }

                $.ajax({
                    url: '<?php echo url_for('spreadsheet_save'); ?>',
                    dataType: 'json',
                    type: 'POST',
                    data: {'data': JSON.stringify(send_data)},
                    success: function (res) {
                        if (res['status'] == 'error') {
                            notify_user(MESSAGE_ERROR, 'Some error occured');
                        }

                        if (res['status'] == 'success' && !res['error_cells']) {
                            notify_user(MESSAGE_SUCCESS, 'All the changes are saved successfully');
                        }

                        if (res['status'] == 'success' && res['error_cells']) {
                            notify_user(MESSAGE_ERROR, 'Few changes are not saved successfully');
                        }

                        if (res['error_cells']) {
                            $.each(res['error_cells'], function (index, element) {
                                status.add_error(element['row'], column_reverse[element['attribute']]);
                            });
                        }
                        if (res['success_cells']) {
                            $.each(res['success_cells'], function (index, element) {
                                status.add_success(element['row'], column_reverse[element['attribute']]);
                            });
                        }

                        $container.handsontable('render');
                    }
                });
            },
            afterRender: function () {
                var instance = $('#container').handsontable('getInstance');
                $.each(status.error_cells, function (index, element) {
                    var cell = instance.getCell(element['row'], element['col']);
                    if (cell) {
                        cell.style.background = error_background;
                    }
                });

                $.each(status.success_cells, function (index, element) {
                    var cell = instance.getCell(element['row'], element['col']);
                    if (cell) {
                        cell.style.background = success_background;
                    }
                });
                $('.play-btn').on('click', function (e) {
                    createPop($(this).attr('href'), 'Play on SoundCloud');
                    e.preventDefault();
                });

            }
        });

        handsontable = $container.data('handsontable');
        loadData();
        calculateHeight();

        $('#notification-row').width($('#navbar-row').width());
    });

    var instance = $('#container').handsontable('getInstance');

    function calculateHeight() {
        var containerOffset = $('#container').offset();
        return $(window).height() - containerOffset['top'] - 11;
        // {# 11px for the horizontal bar #}
    }

    function resizeHook() {
        $("#container").handsontable({
            height: calculateHeight()
        });
        $('#notification-row').width($('#navbar-row').width());
    }

    function generateSuccessAlert(message) {
        return '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>';
    }

    function generateErrorAlert(message) {
        return '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>';
    }

    function notify_user(type, message) {
        if (type == MESSAGE_SUCCESS) {
            $('#notification-placeholder').html(generateSuccessAlert(message));
            setInterval(function () {
                $(".alert").alert('close')
            }, 4000);
        } else if (type == MESSAGE_ERROR) {
            $('#notification-placeholder').html(generateErrorAlert(message));
        }
    }

    </script> 
