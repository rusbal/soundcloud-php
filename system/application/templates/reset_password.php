<?php if ($messages): ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
<?php foreach ($messages as $category => $msgs): ?>
    <?php foreach ($message as $msgs): ?>
                    <div class="alert alert-<?php echo $category; ?>"><?php echo $message; ?></div>
    <?php endforeach; ?>
<?php endforeach; ?>
            </div>
        </div>
<?php endif; ?>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="page-header">
                <h2 class="text-center">Reset Password</h2>
            </div>

            <form class="form-horizontal" role="form" method="post" action="<?php echo url_for('reset_password_post'); ?>">
                <div class="form-group">
                    <label for="current_password" class="col-sm-4 control-label">Current Password</label>

                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="current_password" name="current_password"
                               placeholder="Current Password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="new_password" class="col-sm-4 control-label">New Password</label>

                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="new_password" name="new_password"
                               placeholder="New Password">

                        <p class="help-block">Password length must be 6</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="new_password_retype" class="col-sm-4 control-label">Retype New Password</label>

                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="new_password_retype" name="new_password_retype"
                               placeholder="Retype New Password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-primary">Change Password</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
