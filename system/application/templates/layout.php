<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SoundCloud Tag Editor</title>

    <link href="<?php echo res('bootstrap-3.1.1/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">

    <link href="<?php echo res('style.css'); ?>" rel="stylesheet" type="text/css">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo res('bootstrap-3.1.1/ie/ie10-viewport-bug-workaround.js'); ?>"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo res('bootstrap-3.1.1/ie/html5shiv-3.7.1.min.js'); ?>"></script>
    <script src="<?php echo res('bootstrap-3.1.1/ie/respond.min-1.4.2.js'); ?>"></script>
    <![endif]-->
<?php
if (isset($view['extra_header_template'])) {
    require 'system/application/templates/extra_header_'.$view['template'];
}
?>
</head>

<body onresize="if (resizeHook) { resizeHook(); }">

<div id="notification-row" class="row" style="position:absolute; z-index: 100">
    <div id="notification-placeholder" class="col-md-offset-4 col-md-4"></div>
</div>

<div id="navbar-row" class="navbar navbar-default" role="navigation">

    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo url_for('home'); ?>">SoundCloud Tag Editor</a>

        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
            <li><a href="<?php echo url_for('playlists'); ?>">Playlists</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
<?php if (sc_username() != ""): ?>

                    <li class="dropdown">
                    <a href="<?php echo url_for('home'); ?>" class="dropdown-toggle" data-toggle="dropdown">
                            SoundCloud
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                        <li><a href="#">Connected Account: <?php echo sc_username(); ?></a></li>
<?php if (sc_is_connected()): ?>
                            <li><a href="<?php echo url_for('sc_disconnect'); ?>">Disconnect</a></li>
<?php endif ?>
                        </ul>
                    </li>

<?php endif ?>

<?php if ($data['user']['is_authenticated']): ?>

                    <li class="dropdown">
                    <a href="<?php echo url_for('home'); ?>" class="dropdown-toggle" data-toggle="dropdown">
<?php echo $data['user']['nickname']; ?>
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                        <li><a href="<?php echo url_for('reset_password'); ?>">Reset Password</a></li>
                        <li><a href="<?php echo url_for('logout'); ?>">Logout</a></li>
                        </ul>
                    </li>

<?php endif ?>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>

</div>


<div class="container">
<?php if ($messages): ?>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
<?php foreach ($messages as $category => $message): ?>
<?php foreach ($msgs as $message): ?>
                        <div class="alert alert-<?php echo $category; ?>"><?php echo $message; ?></div>
<?php endforeach ?>
<?php endforeach ?>
                </div>
            </div>
<?php endif ?>

<?php
if (isset($view['template'])) {
    require 'system/application/templates/'.$view['template'];
}
?>

</div>
<!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="<?php echo res('jquery-1.11.0/jquery-1.11.0.min.js'); ?>"></script>
<script src="<?php echo res('bootstrap-3.1.1/js/bootstrap.min.js'); ?>"></script>
<?php
if (isset($view['finishing_block_template'])) {
    require 'system/application/templates/finishing_block_'.$view['template'];
}
?>
</body>
</html>
