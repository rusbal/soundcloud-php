<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SoundCloud Tag Editor</title>

    <link href="<?php echo res('bootstrap-3.1.1/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">

    <link href="<?php echo res('style.css'); ?>" rel="stylesheet" type="text/css">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo res('bootstrap-3.1.1/ie/ie10-viewport-bug-workaround.js'); ?>"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo res('bootstrap-3.1.1/ie/html5shiv-3.7.1.min.js'); ?>"></script>
    <script src="<?php echo res('bootstrap-3.1.1/ie/respond.min-1.4.2.js'); ?>"></script>
    <![endif]-->
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #eee;
        }

        .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }

        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }

        .form-signin .checkbox {
            font-weight: normal;
        }

        .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
        }

        .form-signin .form-control:focus {
            z-index: 2;
        }

        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

    </style>
</head>

<body>

<div class="container">

<?php if ($messages): ?>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
<?php foreach ($messages as $category => $msgs): ?>
    <?php foreach ($msgs as $message): ?>
                        <div class="alert alert-<?php echo $category; ?>"><?php echo $message; ?></div>
    <?php endforeach; ?>
<?php endforeach; ?>
                </div>
            </div>
<?php endif; ?>

<?php if ($error_code  == 'invalid_login'): ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 alert alert-danger text-center">
                Invalid credentials.
            </div>
        </div>
<?php endif; ?>

    <div class="row">
        <div class="col-md-12">

        <form class="form-signin" role="form" action="<?php echo url_for('login_user_pass'); ?>" method="post">
                <h2 class="form-signin-heading">Please sign in</h2>
                <input id="email" name="email" type="email" class="form-control" placeholder="Email address" required>
                <input id="password" name="password" type="password" class="form-control" placeholder="Password"
                       required autofocus>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div>
    </div>

</div>
<!-- /container -->
</body>
</html>
